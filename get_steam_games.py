#!/usr/bin/env python

import requests
import os
from prettytable import PrettyTable

userids = {"Tim": "76561198009670523",
           "Steve": "76561197998718373",
           "Will": "76561198033520378",
           "Evan": "76561198025182865"}

games_owned = {"Tim": {}, "Steve": {}, "Will": {}, "Evan": {}}

all_games = dict()

games_owned_url = 'http://api.steampowered.com/IPlayerService/GetOwnedGames/v0001/?key={}&steamid={}&format=json&include_appinfo=1'

apikey = os.getenv("STEAMAPIKEY")


def unique_game_ids(list_of_games):
    ids = set()
    for game in list_of_games:
        ids.add(game["appid"])
    return ids


if __name__ == "__main__":

    for user in userids:
        r = requests.get(games_owned_url.format(apikey, userids[user]))
        if r.status_code != 200:
            print("Could not get game info for {}".format(user))
            continue
        for game in r.json()['response']['games']:
            games_owned[user][game["appid"]] = game["name"]

    all_games = {**games_owned["Tim"],
                 **games_owned["Steve"],
                 **games_owned["Will"],
                 **games_owned["Evan"]}
    #print(json.dumps(games_owned, indent=4, sort_keys=True))
    #print(json.dumps(all_games, indent=4, sort_keys=True))

    communal = (set(games_owned["Tim"])
                & set(games_owned["Steve"])
                & set(games_owned["Will"])
                & set(games_owned["Evan"]))

    print("Shared games:\n\n")
    [print(all_games[appid]) for appid in communal]

    t = PrettyTable(["Game", "Tim Owns", "Steve Owns", "Will Owns", "Evan Owns"])
    for game in all_games:
        t.add_row([all_games[game],
                   game in games_owned["Tim"],
                   game in games_owned["Steve"],
                   game in games_owned["Will"],
                   game in games_owned["Evan"]])
    print("\n\n")
    print("Game ownership table: \n\n\n")
    print(t)
